import Head from "next/head";

export const getServerSideProps =  async (ctx)=>{
    const baseUrl = process.env.ENVIRONMENT == "DEV"? "https://mylingotalk.com" : "https://lingotalk.co";

    return {props:{baseUrl}};
}

export default function Unauthorized({baseUrl}){
    return (
        <>
            <Head>
                <title>LingoTalk Classroom</title>
                <link rel="icon" href="/favicon.png" />
            </Head>
            <div style={styles.centerized}>
                <div style={styles.textCentered}>
                    <h1 style={styles.bold}>You are currently not logged in to LingoTalk</h1>
                    <p style={styles.muted}>If you are sure you already logged in, please re log-in via LingoTalk, and access this classroom via your dashboard </p>
                    <div style={styles.centerButton}>
                        <a href={baseUrl} style={styles.textCentered} target="_blank" rel="noreferrer">
                            <button style={{ ...styles.button, ...styles.tangerine }}>
                                Back to LingoTalk
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </>
    )
}

const styles = {
    centerized:{
        display:"flex",
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        height:"100vh"
    },
    centerButton:{
        display:"flex",
        justifyContent:"center"
    },
    textCentered:{
        textAlign:"center"
    },
    bold:{
        fontWeight:"bold"
    },
    muted:{
        color:'#777777'
    },
    button: {
        width: '200px',
        height: '50px',
        backgroundColor: '#fefefe',
        color: 'white',
        border: 'none',
        borderRadius: '5px',
        fontSize: '20px',
        fontWeight: 'bold',
        cursor: 'pointer',
        display: 'block',
    },
    tangerine: {
        backgroundColor: '#F28500',
        marginTop: 40,
    }
}