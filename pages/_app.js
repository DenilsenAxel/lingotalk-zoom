import '../styles/globals.css'


function MyApp({ Component, pageProps }) {

  // menghilangkan console.log di production
  if (process.env.NODE_ENV !== 'development') {
    console.log = () => { }
  } 


  return (
    <Component {...pageProps} />
  )
}

export default MyApp
