import Router from 'next/router';
import { useEffect } from 'react';
import axios from "axios";
import Image from "next/image";
import Head from "next/head";

export const getServerSideProps = async(ctx) => {
    const { req, query } = ctx;
    const { cookies } = req;
    const { appointmentId, type } = query;
    const { token } = cookies;
    
    const baseUrl = process.env.ENVIRONMENT == "DEV" ? "https://mylingotalk.com" : "https://lingotalk.co";
    const finishRequestUrl = 
            type == "private" ? `${baseUrl}/api/appointments/finish/${appointmentId}`
            : type == "events" ? `${baseUrl}/api/event_classes/event_appointments/finish/${appointmentId}`
            : `${baseUrl}/api/appointments/finish/${appointmentId}` ;
        console.log(finishRequestUrl);

    if (!token) {
        return {
            redirect: {
                destination: '/unauthorized',
            }
        }
    }

    const {content:zoomInfo} = await axios.get(
            `${baseUrl}/api/zoom/meetingInfo/${appointmentId}`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then((res) => {
            return res.data})
          .catch((err) => console.log('err', err));
    
    if(zoomInfo.user.role == 1){
        
        await axios.put(
                    finishRequestUrl, {
                        headers: {
                            Authorization: `Bearer ${token}`,
                        },
                    }
                )
                .then((res) => {
                    return res.data
                })
                .catch((err) => console.log('err', err));
    }

    return { props: {zoomInfo,baseUrl} };

}


// redirect ke halaman utama ex: localhost:3000/
export default function Leave({zoomInfo,baseUrl}) {

    let text;
    let subText;

    if(zoomInfo.user.role == 1){
        text = "Class finished !"
        subText = "You can now go back to LingoTalk's dashboard"
    }
    else{
        text="Yeay, you have finished a class today!"
        subText = "Now go apply and show your new skill to the rest of the world."
    }

    return (
        <>
        <Head>
            <title>LingoTalk Classroom</title>
            <link rel="icon" href="/favicon.png" />
        </Head>
        <div style={styles.centerized}>
            
            <div style={styles.textCentered}>
                <Image width={480} height={356} src="/easy-setup.gif" alt="Illustration" />
                <h1 style={styles.bold}>{text}</h1>
                <p style={styles.muted}>{subText}</p>
                <div style={styles.centerButton}>
                    <a href={baseUrl} style={styles.textCentered} target="_blank" rel="noreferrer">
                        <button style={{ ...styles.button, ...styles.tangerine }}>
                            Back to LingoTalk
                        </button>
                    </a>
                </div>
            </div>
        </div>
        </>
    )
}

const styles = {
    centerized:{
        display:"flex",
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        height:"100vh"
    },
    centerButton:{
        display:"flex",
        justifyContent:"center"
    },
    textCentered:{
        textAlign:"center"
    },
    bold:{
        fontWeight:"bold"
    },
    muted:{
        color:'#777777'
    },
    button: {
        width: '200px',
        height: '50px',
        backgroundColor: '#fefefe',
        color: 'white',
        border: 'none',
        borderRadius: '5px',
        fontSize: '20px',
        fontWeight: 'bold',
        cursor: 'pointer',
        display: 'block',
    },
    tangerine: {
        backgroundColor: '#F28500',
        marginTop: 40,
    }
}