import { NextApiRequest, NextApiResponse } from 'next';
import crypto from 'crypto';

function generateSignature(apiKey, apiSecret, meetingNumber, role = 1) {
  // Prevent time sync issue between client signature generation and zoom
  const timestamp = new Date().getTime() - 30000;
  const msg = Buffer.from(apiKey + meetingNumber + timestamp + role).toString(
    'base64'
  );
  const hash = crypto
    .createHmac('sha256', apiSecret)
    .update(msg)
    .digest('base64');
  const signature = Buffer.from(
    `${apiKey}.${meetingNumber}.${timestamp}.${role}.${hash}`
  ).toString('base64');

  return signature;
}

const get = (req, res) => {
  if (req.method === 'POST') {
    const { meetingNumber, role, envApiKey, envApiSecret } = req.body;
    console.log('ini query======================', req.body);
    console.log(process.env[envApiKey]);
    console.log(process.env[envApiSecret]);
    const signature = generateSignature(
      process.env[envApiKey],
      process.env[envApiSecret],
      meetingNumber.toString(),
      role.toString()
    );
    return res
      .status(200)
      .json({ signature: signature, apiKey: process.env[envApiKey] });
  }

  return res.status(400).send(`${req.method} Bad request`);
};

export default get;
