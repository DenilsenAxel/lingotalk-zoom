import Head from 'next/head';

export const getServerSideProps =  async (ctx)=>{
    const { req, res , query} = ctx
    const {cookies} = req;
    const token = cookies.token;
    const {appointmentId} = query;
    const baseUrl = process.env.ENVIRONMENT == "DEV"? "https://mylingotalk.com" : "https://lingotalk.co";
    if(!token){
      return {
        redirect: {
          destination: '/unauthorized',
        }
      }
    }

    return {props:{appointmentId,baseUrl}}
}

export default function MeetingUnavailable({appointmentId,baseUrl}){
    return (
        <>
            <Head>
                <title>LingoTalk Classroom</title>
                <link rel="icon" href="/favicon.png" />
            </Head>
            <div style={styles.centerized}>
                <div style={styles.textCentered}>
                    <h1 style={styles.bold}>{`The class is not yet available`}</h1>
                    <p style={styles.muted}>If you are a tutor, please start the meeting via LingoTalk Main Website (lingotalk.co) first</p>
                    <p style={styles.muted}>If you are a student, please wait for the tutor to start the classroom meeting</p>
           
                    <div style={styles.centerButton}>
                        <a href={`/?appointmentId=${appointmentId}`} style={{...styles.textCentered,marginRight:20}}>
                            <button style={{ ...styles.button, ...styles.lingoBlue }}>
                                Try the meeting page again
                            </button>
                        </a>
                        <a href={baseUrl} style={styles.textCentered} target="_blank" rel="noreferrer">
                            <button style={{ ...styles.button, ...styles.tangerine }}>
                                Back to LingoTalk
                            </button>
                        </a>
                        
                    </div>
                     
                </div>
            </div>
        </>
    )
}

const styles = {
    centerized:{
        display:"flex",
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        height:"100vh"
    },
    centerButton:{
        display:"flex",
        justifyContent:"center"
    },
    textCentered:{
        textAlign:"center"
    },
    bold:{
        fontWeight:"bold"
    },
    muted:{
        color:'#777777',
        fontSize:'18px',
        fontWeight:'bold'
    },
    button: {
        minWidth: '200px',
        height: '50px',
        backgroundColor: '#fefefe',
        color: 'white',
        border: 'none',
        borderRadius: '5px',
        fontSize: '20px',
        fontWeight: '500',
        paddingLeft:'20px',
        paddingRight:'20px',
        cursor: 'pointer',
        display: 'block',
    },
    tangerine: {
        backgroundColor: '#F28500',
        marginTop: 40,
    },
    lingoBlue:{
    backgroundColor:'#172e7c',
      marginTop: 40,
  },
}