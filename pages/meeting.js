import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Spinner from '../components/Spinner';
import axios from 'axios';
import Head from "next/head";
import dynamic from "next/dynamic";

export const getServerSideProps =  async (ctx)=>{
    const { req, res , query} = ctx

    const {cookies} = req;

    const token = cookies.token;
    const baseUrl = process.env.ENVIRONMENT == "DEV"? "https://mylingotalk.com" : "https://lingotalk.co";
    const {content:zoomInfo} = await axios.get(
            `${baseUrl}/api/zoom/meetingInfo/${query.appointmentId}`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then((res) => {
            return res.data})
          .catch((err) => console.log('err', err));
    const appointmentId = query.appointmentId;
    return {props:{zoomInfo,appointmentId}}
}


export default function Meeting({zoomInfo, appointmentId}) {
    // konfigurasi
    const [loading, setLoading] = useState(false);
    const { query } = useRouter();
  

    useEffect(() => {
        if (Object.keys(query).length === 0) return;
        setLoading(true);
        let sign = '';
        let config = zoomInfo;

        let API_KEY = '';
        const getSignature = async() => {
            if (config.user) {
                const body = {
                    meetingNumber: config.meetingInfo.meetingId,
                    role: config.user.role,
                    envApiKey: config.zoomAccount.envApiKey,
                    envApiSecret: config.zoomAccount.envApiSecret,
                };
                const { signature, apiKey } = await axios
                    .post(`/api/signature`, body)
                    .then((res) => {
                        return res.data;
                    })
                    .catch((err) => console.log('err', err));
                sign = signature;
                API_KEY = apiKey;
            } else {
                alert('User Not Authorized');
            }
        };

        getSignature();

        // mengimpor modul
        const loadZoom = async() => {
            if (typeof window !== 'undefined') {
                const {ZoomMtg} = await(import('@zoomus/websdk'));

                // ZoomMtg.setZoomJSLib('https://source.zoom.us/2.0.1/lib', '/av'); // CDN version default
                const zoomJSLib = 'https://source.zoom.us/2.1.1/lib';
                const zoomJSAVLib = '/av'
                ZoomMtg.setZoomJSLib(zoomJSLib, zoomJSAVLib);
                await ZoomMtg.preLoadWasm();
                ZoomMtg.prepareWebSDK();

                function joinZoom() {
                    ZoomMtg.init({
                        debug: process.env.NODE_ENV === 'production' ? false : true,
                        leaveUrl: `/leave?type=${config.meetingInfo.type}&appointmentId=${appointmentId}`,
                        // disablePreview: false, // default false
                        success: function() {
                            // menghilangkan spinner
                            setLoading(false);

                            ZoomMtg.i18n.load('en-US');
                            ZoomMtg.i18n.reload('en-US');
                            ZoomMtg.join({
                                meetingNumber: config.meetingInfo.meetingId,
                                userName: config.user.name,
                                signature: sign,
                                passWord: config.meetingInfo.encryptedPassword,
                                apiKey: API_KEY,
                                success: function(res) {
                                    ZoomMtg.getAttendeeslist({});
                                    ZoomMtg.getCurrentUser({
                                        success: function(res) {
                                            console.log(
                                                'success getCurrentUser',
                                                res.result.currentUser
                                            );
                                        },
                                    });
                                },
                                error: function(res) {
                                    console.log('error here');
                                    console.log(res);
                                },
                            });
                        },
                        error: function(res) {
                            console.log('error here2');
                            console.log(res);
                        },
                    });

                    ZoomMtg.inMeetingServiceListener('onUserJoin', function(data) {
                        console.log('inMeetingServiceListener onUserJoin', data);
                    });

                    ZoomMtg.inMeetingServiceListener('onUserLeave', function(data) {
                        console.log('inMeetingServiceListener onUserLeave', data);
                    });

                    ZoomMtg.inMeetingServiceListener(
                        'onUserIsInWaitingRoom',
                        function(data) {
                            console.log(
                                'inMeetingServiceListener onUserIsInWaitingRoom',
                                data
                            );
                        }
                    );

                    ZoomMtg.inMeetingServiceListener('onMeetingStatus', function(data) {
                        console.log('inMeetingServiceListener onMeetingStatus', data);
                    });
                }
                setTimeout(() => joinZoom(), 2000);
            }
        };
        setTimeout(() => loadZoom(), 1000);

        console.log('zoom meeting has already loaded');
    }, [query,zoomInfo, appointmentId]);

    return ( <>
        <Head>
        <title> LingoTalk Classroom </title> 
        <link rel = "icon"
        href = "/favicon.png" />
        </Head> 
        <div style = { styles.center } >
          { loading ? < Spinner /> : null } 
        </div> 
        </>
    );
}

const styles = {
    center: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        width: '100vw',
        overflow: 'none',
    },
};