import axios from 'axios';

export default async function api(req, res) {
    const { method } = req;

    switch (method) {
        case "GET":
            // token JWT dari zoom
            const API_KEY = process.env.NEXT_PUBLIC_API_KEY

            const JWT_TOKEN = `Bearer ${process.env.NEXT_PUBLIC_JWT_TOKEN}`;

            // konfigurasi meeting
            const body = {
                topic: 'Belajar',
            };

            const headers = {
                headers: {
                    authorization: JWT_TOKEN,
                },
            };


            const data = await axios.post("https://api.zoom.us/v2/users/me/meetings", body, headers).then(res => {
                // mendapatkan data meeting dan memasukkanya ke const data
                return res.data
            }).catch(err => console.log("err", err))

            console.log('ini data', data)
            res.status(200).json({ data })
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }

}