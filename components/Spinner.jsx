import s from '../styles/Home.module.css';

const Spinner = () => (
    <svg stroke="#97c2ed" className={s.spinner} viewBox="0 0 50 50">
        <circle
            className={s.path}
            cx="25"
            cy="25"
            r="20"
            fill="none"
            strokeWidth="5"
        ></circle>
    </svg>
);

export default Spinner;