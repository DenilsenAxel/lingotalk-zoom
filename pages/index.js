import { useState, useEffect } from 'react';
import router, { useRouter } from 'next/router';
import Spinner from '../components/Spinner';
import axios from 'axios';
import Head from 'next/head';
import Image from 'next/image';

export const getServerSideProps =  async (ctx)=>{
    const { req, res , query} = ctx
    const {cookies} = req;
    const token = cookies.token;

    if(!token){
      return {
        redirect: {
          destination: '/unauthorized',
        }
      }
    }
    const {appointmentId} = query;
    try{  
   
      const baseUrl = process.env.ENVIRONMENT == "DEV"? "https://mylingotalk.com" : "https://lingotalk.co";
      const {content:zoomInfo} = await axios.get(
            `${baseUrl}/api/zoom/meetingInfo/${appointmentId}`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            }
          )
          .then((res) => {
            return res.data})
          .catch((err) => {
            return null;
          });
      return {props:{zoomInfo, appointmentId}}
    }
    catch(err){
        return {
          redirect:{
            destination : `/meeting-unavailable?appointmentId=${appointmentId}`
          }
        }
    }

}


export default function Home({zoomInfo, appointmentId}) {
  // konfigurasi
  const [loading, setLoading] = useState(true);
  const userRole = zoomInfo.user.role;
  const zoomUrl = userRole == 1 ? zoomInfo.meetingInfo.start_url : zoomInfo.meetingInfo.join_url;
  const router = useRouter();
  useEffect(()=>{
    setLoading(false);
  },[loading])
  
  return (
    <>
      <Head>
        <title>LingoTalk Classroom</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      <div style={styles.center}>
          <div>
            {loading ? <Spinner /> : null}
            {!loading ? (
              <>
                <div style={styles.centeredInCenter}>
                  <Image src="/logo-new.svg" height={59} width={291} alt="Logo"/>
                  <h1 style={styles.classRoomText}>Classroom</h1>
                </div>
                <div style={styles.centeredInCenter}>
                  <a href={zoomUrl} target="_blank" rel="noreferrer">
                    <button style={{ ...styles.button, ...styles.lingoBlue }}>
                      Join Via Desktop
                    </button>
                  </a>
                </div>
                <div style={styles.centeredInCenter}>
                  <a onClick={() => router.push(`/meeting?appointmentId=${appointmentId}`)}>
                    <button style={{ ...styles.button, ...styles.tangerine }}>
                      Join Via Lingotalk
                    </button>
                  </a>
                </div>
              </>
            ) : null}
          </div>
      </div>
    </>
  );
}

const styles = {
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh',
    width: '100vw',
    overflow: 'none',
  },
  button: {
    width: '200px',
    height: '50px',
    backgroundColor: '#fefefe',
    color: 'white',
    border: 'none',
    borderRadius: '5px',
    fontSize: '20px',
    fontWeight: 'bold',
    cursor: 'pointer',
    display: 'block',
  },
  tangerine: {
    backgroundColor: '#F28500',
    marginTop: 40,
  },
  lingoBlue:{
    backgroundColor:'#172e7c',
      marginTop: 40,
  },
  centeredInCenter:{
    display:"flex",
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"center"
  },
  classRoomText:{
    height:59,
    fontWeight:"bold",
    marginLeft:20
  }
};
